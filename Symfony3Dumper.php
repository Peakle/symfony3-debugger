<?php

namespace Debugger\Component\Symfony3Dumper;

class Symfony3Dumper {

    public static function write($data)
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 2000);

        $file = $trace[1]['file'];
        $projectDir = dirname(__DIR__, 3);
        $frel = ltrim(substr($file, \strlen($projectDir)), \DIRECTORY_SEPARATOR);

        $date = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $date = date_format($date, 'H:i:s +3:00');

        $line = $trace[1]['line'];

        $host = 'tcp://127.0.0.1:7777';

        $encodedPayload = base64_encode(serialize([[ 'file' => $frel, 'line' => $line, 'date' => $date], $data ]));

        $socket = self::createSocket($host);

        set_error_handler([self::class, 'nullErrorHandler']);

        try {

            if (-1 !== stream_socket_sendto($socket, $encodedPayload)) {
                return true;
            }

            stream_socket_shutdown($socket, STREAM_SHUT_RDWR);
            fclose($socket);
            $socket = self::createSocket($host);

            if (-1 !== stream_socket_sendto($socket, $encodedPayload)) {
                return true;
            }

        } finally {
            restore_error_handler();
        }

        return false;
    }

    public static function nullErrorHandler($t, $m)
    {
        // no-op
    }

    public static function createSocket($host)
    {
        try {
            return stream_socket_client($host, $errno, $errstr, 3, STREAM_CLIENT_CONNECT);
        } finally {
            restore_error_handler();
        }
    }
}
