<?php

namespace Debugger\Component\Symfony3Dumper\Command;

use \Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Style\SymfonyStyle;

class DebugCommand extends Command
{
    private $host;
    private $socket;
    private $io;
    protected static $defaultName = 'server:debug';

    protected function configure()
    {
        $this->setName('server:debug');
        $this->setDescription('debug php');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->host = 'tcp://127.0.0.1:7777';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $output->writeln('<info>Listen 127.0.0.1:7777</info>');
        $output->writeln('<info>Enter ctrl+c for exit command</info>');
        $this->listen(function ($payload) {
            $this->io->writeln("\e[1;32m-----------------\nSource - {$payload[0]['file']} on line {$payload[0]['line']}\nDate - {$payload[0]['date']}\n-----------------\n\e[0m\n");
            dump($payload[1]);
            $this->io->newLine();
        });
    }

    private function start(): void
    {
        $this->socket = @stream_socket_server($this->host, $errno, $errstr);
    }

    private function listen(callable $callback): void
    {
        if (null === $this->socket) {
            $this->start();
        }

        if (\is_resource($this->socket)) {
            foreach ($this->getMessages() as $clientId => $message) {

                $payload = @unserialize(base64_decode($message));

                if (false === $payload) {
                    dump("Unable to decode a message from client {$clientId}");
                    continue;
                }

                if (!\is_array($payload) || \count($payload) < 2) {
                    dump("Invalid payload from {$clientId} client. Expected an array of two elements");
                    continue;
                }

                $callback($payload);
            }
        } else {
            echo "\n";
            echo "\033[01;31m Can't open port, check firewall policy \033[0m\n";
            exit();
        }
    }

    private function getMessages(): iterable
    {
        $sockets = [(int)$this->socket => $this->socket];
        $write = [];

        while (true) {
            $read = $sockets;
            stream_select($read, $write, $write, null);

            foreach ($read as $stream) {
                if ($this->socket === $stream) {
                    $stream = stream_socket_accept($this->socket);
                    $sockets[(int)$stream] = $stream;
                } elseif (feof($stream)) {
                    unset($sockets[(int)$stream]);
                    fclose($stream);
                } else {
                    yield (int)$stream => fgets($stream);
                }
            }
        }
    }

    function __destruct()
    {
        if (\is_resource($this->socket)) {
            socket_close($this->socket);
        }
    }
}
