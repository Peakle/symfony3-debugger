<?php

use Symfony\Component\VarDumper\VarDumper;
use Debugger\Component\Symfony3Dumper\Symfony3Dumper;

if (!function_exists('db')) {

    function db(...$moreVars)
    {
        $host = 'tcp://127.0.0.1:7777';
        $open_socket = true;
        try {
            $ping = @stream_socket_client($host, $errno, $errstr, 3);
        } catch (\Exception $exception) {
            $open_socket = false;
        }


        if (!$open_socket || !is_resource($ping)) {

            foreach ($moreVars as $v) {
                VarDumper::dump($v);
            }

        } else {

            fclose($ping);

            foreach ($moreVars as $v) {
                Symfony3Dumper::write($v);
            }

        }
    }
}

if (!function_exists('dd')) {
    function dd(...$vars)
    {
        $host = 'tcp://127.0.0.1:7777';
        $open_socket = true;

        try {
            $ping = @stream_socket_client($host, $errno, $errstr, 3);
        } catch (\Exception $exception) {
            $open_socket = false;
        }

        if (!$open_socket || !is_resource($ping)) {

            foreach ($vars as $v) {
                VarDumper::dump($v);
            }

        } else {

            fclose($ping);

            foreach ($vars as $v) {
                Symfony3Dumper::write($v);
            }

        }

        die(1);
    }
}